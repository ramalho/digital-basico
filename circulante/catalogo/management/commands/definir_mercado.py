# coding: utf-8

import io
import sys

from django.core.management.base import BaseCommand, CommandError

from catalogo.models import Publicacao, Credito
from catalogo import isbn

class Command(BaseCommand):
    args = '[mudar_tipo]'
    help = u'Define o mercado editorial conforme o ISBN-10'

    def handle(self, *args, **options):
        qtd_alterados = 0
        for pub in Publicacao.objects.filter(tipo='livro'):
            lang = isbn.convertISBN13toLang(
                       isbn.convertISBN10toISBN13(pub.id_padrao))
            if lang is not None:
                pub.mercado = lang
                pub.save()
                qtd_alterados += 1

        self.stdout.write(u'%s registros alterados\n' % qtd_alterados)












