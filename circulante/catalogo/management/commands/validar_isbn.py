# coding: utf-8

# XXX: melhorar tratamento de errros
#      - encoding inexistente
#      - nao conseguimos ler o arquivo

import io
import sys

from django.core.management.base import BaseCommand, CommandError

from catalogo.models import Publicacao, Credito
from catalogo import isbn

class Command(BaseCommand):
    args = '[mudar_tipo]'
    help = u'Verifica se Publicacao.id_padrao é um ISBN-10 válido'

    def handle(self, *args, **options):
        qtd = 0
        qtd_alterados = 0
        for pub in Publicacao.objects.all():
            if isbn.validatedISBN10(pub.id_padrao) is None:
                linha = u'{0:6} {1:16} {2}\n'.format(pub.id, 
                                                  pub.id_padrao,
                                                  pub.titulo[:50])
                self.stdout.write(linha)
                qtd += 1
            elif 'mudar_tipo' in args:
                pub.tipo = 'livro'
                pub.save()
                qtd_alterados += 1
        self.stdout.write(u'%s registros com ISBN inválido\n' % qtd)
        if 'mudar_tipo' in args:
            self.stdout.write(u'%s registros alterados\n' % qtd_alterados)












