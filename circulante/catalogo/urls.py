# coding: utf-8

from django.conf.urls import patterns, include, url

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from .models import Publicacao

cds = Publicacao.objects.filter(tipo='midia')

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'circulante.views.home', name='home'),
    # url(r'^circulante/', include('circulante.foo.urls')),
    url(r'^hora/', 'catalogo.views.hora'),
    url(r'^horad/(\d{1,2})/', 'catalogo.views.hora_delta'),
    url(r'^cds/', 'catalogo.views.listar_cds', name='lista_cds'),
    # FIXME: porque pubs/livro e pubs/n/d/ funcionam mas pubs/n/d dá 404?
    url(r'^pubs/([\w/]+)/', 'catalogo.views.listar_pubs', name='lista_pubs_tipo'),
    url(r'^pubs/$', 'catalogo.views.listar_pubs', name='lista_pubs_OLD'),
    url(r'^pub/(\d+)/', 'catalogo.views.detalhe_pub', name='detalhe_pub_OLD'),
    # url(r'^hora/', hora),
    # exemplos de views genericas
    # FIXME: implementar paginacao
    # FIXME: integrar Bootstrap nos templates
    url(r'^gpubs/$', ListView.as_view(model=Publicacao), name='lista_pubs'),
    url(r'^gpub/(?P<pk>\d+)/', DetailView.as_view(model=Publicacao), name='detalhe_pub'),
    url(r'^gcds/$', ListView.as_view(queryset=cds), name='lista_cds'),
)
