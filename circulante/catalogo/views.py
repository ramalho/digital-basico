# Create your views here.

import time
import datetime

from django.http import HttpResponse
from django.shortcuts import render
from django.http import Http404

from .models import Publicacao, TIPO_PUBLICACAO

def listar_cds(request):
    return listar_pubs(request, 'midia')

def listar_pubs(request, tipo=None):
    if tipo is None:
        pubs = Publicacao.objects.all()
        nome_tipo = 'Todos os tipos'
    else:
        tipos = dict(TIPO_PUBLICACAO)
        if tipo not in tipos:
            raise Http404
        pubs = Publicacao.objects.filter(tipo=tipo).order_by('titulo')
        nome_tipo = tipos[tipo]
    vars_dict = {'tipo':nome_tipo, 'object_list':pubs}
    return render(request, 'catalogo/publicacao_list.html', vars_dict)

def detalhe_pub(request, digitos):
    try:
        pub_id = int(digitos) # FIXME: tratar id nao numerico
        pub = Publicacao.objects.get(id=pub_id) # FIXME: tratar id inexistente
    except (OverflowError, Publicacao.DoesNotExist):
        raise Http404

    return render(request, 'catalogo/detalhe_pub.html', {'pub':pub})

def hora(request):
    hms = time.strftime('%H:%M:%S')
    return HttpResponse(hms)

def hora_delta(request, delta):
    delta = int(delta)
    delta = datetime.timedelta(hours=delta)
    hora = datetime.datetime.now()+delta
    hora = hora.strftime('%H:%M:%S')
    html = "<html><body><h1>%s</h1></body></html>" % hora
    return HttpResponse(html)

