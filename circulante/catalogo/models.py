# coding: utf-8

from django.db import models

TIPO_PUBLICACAO = [
    ('n/d', u'não informado'),
    ('livro', u'livro'),
    ('midia', u'CD/DVD/BluRay'),
    ('apostila', u'apostila'),
    ('outro', u'outro'),
]

MERCADOS_EDITORIAIS = [
    ('--', u'não identificado'),
    ('en', u'inglês'),
    ('fr', u'francês'),
    ('de', u'alemão'),
    ('jp', u'japonês'),
    ('ru', u'russo'),
    ('es', u'espanhol'),
    ('pt', u'lusófono'),    
]

class Publicacao(models.Model):
    titulo = models.CharField(max_length=128)
    id_padrao = models.CharField(max_length=16, blank=True)
    tipo = models.CharField(max_length=16,
                            choices=TIPO_PUBLICACAO,
                            default='n/d')
    editora = models.CharField(max_length=64, blank=True)
    num_paginas = models.PositiveIntegerField(default=0)
    mercado = models.CharField(max_length='2', 
        choices=MERCADOS_EDITORIAIS, default='--')
    
    class Meta:
        verbose_name = u'Publicação'
        verbose_name_plural = u'Publicações'
        
    def __unicode__(self):
        return self.titulo

    @models.permalink
    def get_absolute_url(self):
        # hardcoded seria assim:
        # return '/cat/pub/%s/' % self.id
        return ('detalhe_pub', [self.id])


class Credito(models.Model):
    nome = models.CharField(max_length=128)
    papel = models.CharField(max_length=32, blank=True)
    publicacao = models.ForeignKey(Publicacao)
    pessoa = models.ForeignKey('Pessoa', null=True, blank=True)
    
    def __unicode__(self):
        return u'{0.nome} em {1.titulo}'.format(self, self.publicacao)

    
class Pessoa(models.Model):
    nome = models.CharField(max_length=128)
    nascimento = models.CharField(max_length=16, blank=True)
    falecimento = models.CharField(max_length=16, blank=True)
    


