# coding: utf-8

from django.contrib import admin

from .models import Publicacao, Credito, Pessoa

class CreditoInline(admin.TabularInline):
    model = Credito
    exclude = ['pessoa']

class PublicacaoAdmin(admin.ModelAdmin):
    list_display = ['id', 'id_padrao', 'tipo', 'titulo']
    list_display_links = ['id', 'id_padrao', 'titulo']
    list_filter = ['tipo', 'mercado']
    search_fields = ['titulo']
    list_editable = ['tipo']
    inlines = [
        CreditoInline,
    ]

class CreditoAdmin(admin.ModelAdmin):
    list_display = ['id', 'nome', 'titulo_publicacao']
    search_fields = ['nome', 'publicacao__titulo']

    def titulo_publicacao(self, obj):
        return obj.publicacao.titulo
    titulo_publicacao.short_description = u'Título da publicação'


admin.site.register(Publicacao, PublicacaoAdmin)
admin.site.register(Credito, CreditoAdmin)
admin.site.register(Pessoa)
